import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import config from '../config'
import VueAxiosPlugin from 'vue-axios-plugin'

Vue.config.productionTip = false

Vue.use(VueAxiosPlugin, {
  baseURL: config.siteUrl + 'wp-json/wp/v2'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
