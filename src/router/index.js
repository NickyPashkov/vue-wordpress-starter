import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import config from '../../config.js'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Home
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.NODE_ENV === 'production' ? config.base : '/',
  routes
})

export default router
