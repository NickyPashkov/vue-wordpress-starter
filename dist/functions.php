<?php

@ini_set('upload_max_size' , '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');

function load_vue_resources() {
	wp_enqueue_style('app', get_template_directory_uri().'/css/app.css');

	wp_enqueue_script('chunk-vendors', get_template_directory_uri() . '/js/chunk-vendors.js', array (), null, true);
  wp_enqueue_script('app', get_template_directory_uri() . '/js/app.js', array (), null, true);
}
add_action('wp_enqueue_scripts', 'load_vue_resources');