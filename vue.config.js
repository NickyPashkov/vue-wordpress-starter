module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  filenameHashing: false,
  productionSourceMap: false,
  chainWebpack: config => {
    if(process.env.NODE_ENV === 'production') {
      config.plugins.delete('html')
      config.plugins.delete('preload')
      config.plugins.delete('prefetch')
    }
  }
}